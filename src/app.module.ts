import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { TopicsModule } from './topics/topics.module';
import { MatchesModule } from './matches/matches.module';

@Module({
  imports: [ConfigModule.forRoot(), TopicsModule, MatchesModule],
})
export class AppModule {}
