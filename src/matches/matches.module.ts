import { Module } from '@nestjs/common';

import { MatchesGateway } from './matches.gateway';
import { MatchesService } from './matches.service';
import { TopicsModule } from '../topics/topics.module';
import { MatchesController } from './matches.controller';

@Module({
  imports: [TopicsModule],
  providers: [MatchesGateway, MatchesService],
  exports: [MatchesService],
  controllers: [MatchesController],
})
export class MatchesModule {}
