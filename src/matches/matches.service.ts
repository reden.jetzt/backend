import { Injectable, OnModuleInit } from '@nestjs/common';

import staticNames from './names.json';
import { UserDto } from './dto/user.dto';
import { SignupDto } from './dto/signup.dto';
import { TopicsService } from '../topics/topics.service';

@Injectable()
export class MatchesService implements OnModuleInit {
  userpool: { [key: string]: UserDto } = {};
  totalConnections = 0;
  randomNames: string[] = [];

  constructor(private topicsService: TopicsService) {}

  onModuleInit() {
    this.randomNames = staticNames;
  }

  getRandomName() {
    return this.randomNames[
      Math.floor(Math.random() * this.randomNames.length)
    ];
  }

  increaseTotalConnections() {
    ++this.totalConnections;
  }

  decreaseTotalConnections() {
    --this.totalConnections;
  }

  createUser(clientId: string, userData: SignupDto): UserDto {
    const user: UserDto = {
      ...userData,
      clientId,
      name: this.getRandomName(),
      partnerId: null,
      randomConnect: false,
    };

    user.topicIds.map(topicId => {
      this.topicsService.adjustTopicQueue(topicId, 1);
      this.topicsService.adjustTopicTotal(topicId, 1);
    });

    this.userpool[clientId] = user;

    return user;
  }

  deleteUser(clientId: string) {
    const user = this.findUser(clientId);

    if (user) {
      const partnerId = user.partnerId;
      user.topicIds.map(topicId => {
        this.topicsService.adjustTopicTotal(topicId, -1);
        if (!user.partnerId) {
          this.topicsService.adjustTopicQueue(topicId, -1);
        }
      });

      delete this.userpool[clientId];

      this.deleteUser(partnerId);
    }
  }

  findUser(clientId: string): UserDto {
    return this.userpool[clientId];
  }

  findMatch(searchingClientId: string): [UserDto, UserDto | null] {
    const searchingUser = this.findUser(searchingClientId);

    const matchingUserClientId = Object.keys(this.userpool).find(key => {
      const checkingUser: UserDto = this.userpool[key];

      if (checkingUser.partnerId) return false;

      if (checkingUser.clientId === searchingUser.clientId) return false;

      if (searchingUser.randomConnect && checkingUser.randomConnect)
        return true;
        
      const ageRangesMatch =
        searchingUser.ownAge >= checkingUser.minAge &&
        searchingUser.ownAge <= checkingUser.maxAge &&
        checkingUser.ownAge >= searchingUser.minAge &&
        checkingUser.ownAge <= searchingUser.maxAge;
      if (!ageRangesMatch) return false;

      const hasCommonTopic = checkingUser.topicIds.some(id =>
        searchingUser.topicIds.includes(id),
      );
      if (!hasCommonTopic) return false;

      return true;
    });

    const matchingUser = matchingUserClientId
      ? this.findUser(matchingUserClientId)
      : null;

    return [searchingUser, matchingUser];
  }

  matchUsers(searchingClientId: string, matchingClientId: string): void {
    const searchingUser = this.findUser(searchingClientId);
    const matchingUser = this.findUser(matchingClientId);

    searchingUser.partnerId = matchingUser.clientId;
    matchingUser.partnerId = searchingUser.clientId;

    const topicIds = [...searchingUser.topicIds, ...matchingUser.topicIds];
    topicIds.forEach(id => this.topicsService.adjustTopicQueue(id, -1));
  }
}
