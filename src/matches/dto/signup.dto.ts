export class SignupDto {
  peerId: string;
  topicIds: string[];
  minAge: number;
  maxAge: number;
  ownAge: number;
}
