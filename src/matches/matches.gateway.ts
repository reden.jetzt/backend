import { MatchesController } from './matches.controller';
import {
  SubscribeMessage,
  WebSocketGateway,
  MessageBody,
  ConnectedSocket,
  OnGatewayDisconnect,
  WebSocketServer,
  OnGatewayConnection,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';

import { SignupDto } from './dto/signup.dto';
import { ChangeNameDto } from './dto/changeName.dto';
import { MatchesService } from './matches.service';
import { UserDto } from './dto/user.dto';

@WebSocketGateway({ namespace: 'matches' })
export class MatchesGateway
  implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server: Server;

  constructor(private matchesService: MatchesService) {}

  handleConnection() {
    this.matchesService.increaseTotalConnections();
    this.sendTotalConnections();
  }

  handleDisconnect(@ConnectedSocket() socket: Socket) {
    this.matchesService.decreaseTotalConnections();
    this.sendTotalConnections();
    this.matchesService.deleteUser(socket.client.id);
  }

  sendTotalConnections() {
    this.server.emit('totalconnections', {
      totalConnections: this.matchesService.totalConnections,
    });
  }

  @SubscribeMessage('signup')
  handleSignup(
    @MessageBody() data: SignupDto,
    @ConnectedSocket() socket: Socket,
  ): UserDto {
    return this.matchesService.createUser(socket.client.id, data);
  }

  @SubscribeMessage('changename')
  handleChangeName(
    @MessageBody() data: ChangeNameDto,
    @ConnectedSocket() socket: Socket,
  ): UserDto {
    const user = this.matchesService.findUser(socket.client.id);
    user.name = data.name;
    return user;
  }

  // Is triggered by client if waiting longer than 1 minute
  @SubscribeMessage('randomconnect')
  handleRandomConnect(@ConnectedSocket() socket: Socket): UserDto {
    const user = this.matchesService.findUser(socket.client.id);
    user.randomConnect = true;
    return user;
  }

  @SubscribeMessage('findmatch')
  handleMatch(@ConnectedSocket() socket: Socket): void {
    const [searchingUser, matchingUser] = this.matchesService.findMatch(
      socket.client.id,
    );

    if (matchingUser) {
      this.matchesService.matchUsers(
        searchingUser.clientId,
        matchingUser.clientId,
      );

      this.server.sockets[`/matches#${searchingUser.clientId}`].emit(
        'foundmatch',
        {
          isCaller: true,
          partnerPeerId: matchingUser.peerId,
          matchedUserTopics: matchingUser.topicIds,
          ownTopics: searchingUser.topicIds
        },
      );
      this.server.sockets[`/matches#${matchingUser.clientId}`].emit(
        'foundmatch',
        {
          isCaller: false,
          partnerPeerId: searchingUser.peerId,
          matchedUserTopics: searchingUser.topicIds,
          ownTopics: matchingUser.topicIds
        },
      );
    }
  }
}
