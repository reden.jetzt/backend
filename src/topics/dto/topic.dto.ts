export class TopicDto {
  id: string;
  name: string;
  peopleInQueue: number;
  peopleInTotal: number;
  tags: string[];
}
