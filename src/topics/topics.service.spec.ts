import { Test, TestingModule } from '@nestjs/testing';

import { TopicsService } from './topics.service';

describe('TopicsService', () => {
  let service: TopicsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TopicsService],
    }).compile();

    service = module.get<TopicsService>(TopicsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should not be empty', () => {
    service.onModuleInit();

    expect(service.topics.length).toBeGreaterThan(0);
  });

  it('should process correctly', () => {
    const topics = [
      {
        name: 'Origami basteln',
        tags: ['creative'],
      },
    ];

    const processedTopic = service.processStaticTopics(topics)[0];

    expect(processedTopic.peopleInQueue).toBe(0);
    expect(processedTopic.peopleInTotal).toBe(0);
    expect(typeof processedTopic.id).toBe('string');
  });

  it('should find topic', () => {
    const topic = {
      id: 'abc',
      name: 'Origami basteln',
      peopleInQueue: 10,
      peopleInTotal: 20,
      tags: ['creative'],
    };

    service.topics = [topic];

    expect(service.findTopic('abc')).toBe(topic);
  });

  it('shoud adjust topic queue', () => {
    const topic = {
      id: 'abc',
      name: 'Origami basteln',
      peopleInQueue: 10,
      peopleInTotal: 20,
      tags: ['creative'],
    };

    service.topics = [topic];

    expect(service.adjustTopicQueue('abc', 1)).toHaveProperty(
      'peopleInQueue',
      11,
    );
    expect(service.adjustTopicQueue('abc', -2)).toHaveProperty(
      'peopleInQueue',
      9,
    );
  });

  it('should adjust topic total', () => {
    const topic = {
      id: 'abc',
      name: 'Origami basteln',
      peopleInQueue: 10,
      peopleInTotal: 20,
      tags: ['creative'],
    };

    service.topics = [topic];

    expect(service.adjustTopicTotal('abc', 1)).toHaveProperty(
      'peopleInTotal',
      21,
    );
    expect(service.adjustTopicTotal('abc', -2)).toHaveProperty(
      'peopleInTotal',
      19,
    );
  });
});
