import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ExpressPeerServer } from 'peer';
import { AppModule } from './app.module';
import Cors from 'cors';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.getHttpAdapter().options("*", Cors());

  // Peer
  const peerServer = ExpressPeerServer(app.getHttpServer(), {
    debug: process.env.DEBUG_PEER === 'true',
    proxied: true,
    path: ""
  });

  app.use('/peerjs', peerServer);

  // Swagger
  const options = new DocumentBuilder()
    .setTitle(process.env.npm_package_title)
    .setDescription(process.env.npm_package_description)
    .setVersion(process.env.npm_package_version)
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  // Enable CORS
  if (process.env.CORS == 'true') {
    app.enableCors({
      origin: true,
      preflightContinue: true
    });
  }

  await app.listen(process.env.PORT);
}
bootstrap();
