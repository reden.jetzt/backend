FROM node:12-alpine

WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN npm run build

CMD [ "npm", "run", "start:prod" ]

EXPOSE 80
