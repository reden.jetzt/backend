<p align="center">
  <a href="https://reden.jetzt" target="blank"><img src="./reden-jetzt-icon.svg" width="320" alt="Nest Logo" /></a>
</p>


## Description

Gerade in Zeiten vom Coronavirus sind viele (alleine) zuhause und würden sich gerne unterhalten um Einsamkeit und Langeweile entgegenzuwirken. Es gibt schon einige Plattformen die einem eine*n Rederpartner*in vermitteln, doch bei den meisten muss man sich entweder anmelden oder der/die Redepartner*in wird komplett durch Zufall bestimmt. An dieser Stelle setzt [reden.jetzt](https://reden.jetzt) ein.

Entstanden beim [#WirVsVirusHack](https://wirvsvirushackathon.devpost.com/)

Siehe auch unseren [Devpost](https://devpost.com/software/reden-jetzt)
## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## API

View our API [here](http://api.reden.jetzt/api/)

## Support

No. ;)

## License

  reden.jetzt is [MIT licensed](LICENSE).

## Code of conduct

Please read our [code of conduct](CODE_OF_CONDUCT.MD) if you want to contribute.